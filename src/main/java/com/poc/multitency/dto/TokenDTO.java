package com.poc.multitency.dto;
public class TokenDTO {

	private String token;
	private String userName;
	private String role;
	private String tenant;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTenant() {
		return tenant;
	}

	public void setTenant(String tenant) {
		this.tenant = tenant;
	}

	@Override
	public String toString() {
		return "TokenDTO [token=" + token + ", userName=" + userName + ", role=" + role + ", tenant=" + tenant + "]";
	}



}