package com.poc.multitency.service;


import java.util.List;

import com.poc.multitency.domain.Faculty;

public interface FacultyService {

			public Faculty addFaculty(Faculty faculty);
			public List<Faculty> getAllFaculties();
			public void updateFaculty(Faculty faculty);
}
