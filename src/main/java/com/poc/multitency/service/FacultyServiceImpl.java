package com.poc.multitency.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.poc.multitency.dao.FacultyRepo;
import com.poc.multitency.domain.Faculty;

@Service
public class FacultyServiceImpl implements FacultyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FacultyServiceImpl.class);
	@Autowired
	private FacultyRepo facultyRepo;
	


	@Override
	public Faculty addFaculty(Faculty faculty) {
		Assert.notNull(faculty, "Faculty object can not be null");
		
			faculty = facultyRepo.save(faculty);
		
		LOGGER.info("Faculty is added successfuly with id : {}", faculty.getId());
		return faculty;
	}

	@Override
	public List<Faculty> getAllFaculties() {
		List<Faculty> facultyList = facultyRepo.findAll();
		LOGGER.info("Total faculty count is : {}", facultyList != null ? facultyList.size() : 0);
		return facultyList;
	}

	@Override
	public void updateFaculty(Faculty faculty) {
		
				Faculty dbFaculty = facultyRepo.findAll().get(0);
				dbFaculty.setEmail(faculty.getEmail());
				facultyRepo.save(dbFaculty);
	}

}
