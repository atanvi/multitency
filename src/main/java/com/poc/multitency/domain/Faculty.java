package com.poc.multitency.domain;

import org.springframework.data.annotation.Id;

public class Faculty {
	
	@Id
	private String id;
	private String name;
	private String email;
	private String mobile;
	private String qualification;

	public Faculty() {

	}

	private Faculty(FacultyBuilder facultyBuilder) {
		this.name = facultyBuilder.name;
		this.email = facultyBuilder.email;
		this.mobile = facultyBuilder.mobile;
		this.qualification = facultyBuilder.qualification;

	}

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}


	public static class FacultyBuilder {

		private String name;
		private String email;
		private String mobile;
		private String qualification;

		public FacultyBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public FacultyBuilder withEmail(String email) {
			this.email = email;
			return this;
		}

		public FacultyBuilder withMobile(String mobile) {
			this.mobile = mobile;
			return this;
		}

		public FacultyBuilder withQualification(String qualification) {
			this.qualification = qualification;
			return this;
		}

		public Faculty build() {
			return new Faculty(this);
		}

	}

}
