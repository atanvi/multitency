package com.poc.multitency.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.poc.multitency.domain.Faculty;

public interface FacultyRepo extends MongoRepository<Faculty, String> {

}
