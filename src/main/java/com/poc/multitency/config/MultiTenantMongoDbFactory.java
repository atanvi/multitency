package com.poc.multitency.config;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.support.PersistenceExceptionTranslator;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoDbUtils;
import org.springframework.data.mongodb.core.MongoExceptionTranslator;

import com.mongodb.DB;
import com.poc.multitency.config.TenantsConfig.TenantData;

public class MultiTenantMongoDbFactory implements MongoDbFactory {

	private PersistenceExceptionTranslator exceptionTranslator;
	private TenantDataFactory tenantDataFactory;

	public MultiTenantMongoDbFactory(TenantDataFactory tenantDataFactory) {
		this.exceptionTranslator = new MongoExceptionTranslator();
		this.tenantDataFactory = tenantDataFactory;
	}

	@Override
	public DB getDb(String dbName) throws DataAccessException {
		return getDb();
	}

	@Override
	public DB getDb() throws DataAccessException {
		String tenant = TenantContext.getCurrentTenant();
		TenantData tenantData = tenantDataFactory.getTenantData(tenant);
		return MongoDbUtils.getDB(tenantData.getMongoClient(), tenantData.getDbName());
	}

	@Override
	public PersistenceExceptionTranslator getExceptionTranslator() {
		return exceptionTranslator;
	}
}
