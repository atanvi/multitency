package com.poc.multitency.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.poc.multitency.config.TenantsConfig.TenantData;
@ConfigurationProperties(prefix = "app")
public class TenantDataFactory {

	private Map<String, TenantData> tenantDataMap=new HashMap<String, TenantsConfig.TenantData>();
	@Value("${app.defaultTenant}")
	private String defaultTenant;
	
	public TenantDataFactory(Map<String, TenantsConfig.TenantData> tenants) {
		
		for (Map.Entry<String, TenantsConfig.TenantData> entry : tenants.entrySet()) {
			TenantsConfig.TenantData tenantData = entry.getValue();
			tenantDataMap.put(entry.getKey(),
					new TenantData(tenantData.getDbName(),
							new MongoClient(new ServerAddress(tenantData.getHost(),tenantData.getPort()),
									Collections.singletonList(MongoCredential.createCredential(tenantData.getUser(),
											tenantData.getDbName(), tenantData.getPassword().toCharArray())))));
		}
	}

	public TenantData getTenantData(String tenant) {
		TenantData tenantData = tenantDataMap.get(tenant);
		System.out.println("Tenant name :"+tenant);
		if (tenantData == null) {
			tenantData = tenantDataMap.get(defaultTenant);
		
		}
		return tenantData;
	}
}