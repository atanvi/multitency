package com.poc.multitency.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.mongodb.MongoClient;

@Configuration
@ConfigurationProperties(prefix = "app")
@EnableConfigurationProperties
public class TenantsConfig {
    
    
	Map<String, TenantData> tenants = new HashMap<>();
	

	public Map<String, TenantData> getTenants() {
		return tenants;
	}

	public void setTenants(Map<String, TenantData> tenants) {
		this.tenants = tenants;
	}

	public static class TenantData {

		private String dbName;
		private String user;
		private String password;
		private String host;
		private int port;
		
		private MongoClient mongoClient;

		public TenantData() {
			
		}



		public TenantData(String dbName, MongoClient mongoClient) {
			this.dbName = dbName;
			this.mongoClient = mongoClient;
		}

		public String getDbName() {
			return dbName;
		}

		public void setDbName(String dbName) {
			this.dbName = dbName;
		}

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		

		public MongoClient getMongoClient() {
			return mongoClient;
		}

		public void setMongoClient(MongoClient mongoClient) {
			this.mongoClient = mongoClient;
		}



		public String getHost() {
			return host;
		}



		public void setHost(String host) {
			this.host = host;
		}



		public int getPort() {
			return port;
		}



		public void setPort(int port) {
			this.port = port;
		}
	}
}