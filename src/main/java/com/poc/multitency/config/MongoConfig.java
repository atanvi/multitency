package com.poc.multitency.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@Import(TenantsConfig.class)
public class MongoConfig {

	@Bean
	public TenantDataFactory tenantDataFactory(@Autowired TenantsConfig tenantsConfig) {
		return new TenantDataFactory(tenantsConfig.getTenants());
	}
	

	@Bean
	public MongoDbFactory mongoDbFactory(@Autowired TenantDataFactory tenantDataFactory) {
		return new MultiTenantMongoDbFactory(tenantDataFactory);
	}

	@Bean
	public MongoTemplate mongoTemplate(@Autowired MongoDbFactory mongoDbFactory) {
		return new MongoTemplate(mongoDbFactory);
	}
}