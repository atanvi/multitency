package com.poc.multitency;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.poc.multitency.security.domain.MultiTencyUser;
import com.poc.multitency.security.domain.Role;
import com.poc.multitency.security.repo.MultitencyRepo;

@SpringBootApplication
public class MultitencyApplication extends SpringBootServletInitializer implements CommandLineRunner {
	
	
	public static void main(String[] args) {
		SpringApplication.run(MultitencyApplication.class, args);
	}
	

	@Autowired
	ApplicationContext context;
	
	@Override
	public void run(String... args) throws Exception {
		
		MultitencyRepo multitencyRepo=context.getBean(MultitencyRepo.class);

		MultiTencyUser user1=new MultiTencyUser();
		user1.setId("1001");
		user1.setFirstName("Lakshman");
		user1.setEmail("lakshman.a@ncet.com");
		user1.setTenant("ncet");
		user1.setPassword("lakshman");
		Role role=new Role();
		role.setRole("ROLE_USER");
		role.setRoleDescription("Application user");
		user1.setRole(role);
		MultiTencyUser user2=new MultiTencyUser();
		user2.setId("1002");
		user2.setFirstName("Krish");
		user2.setEmail("krish.t@ait.com");
		user2.setTenant("ait");
		user2.setPassword("krish");
		user2.setRole(role);
		multitencyRepo.deleteAll();;
		multitencyRepo.save(Arrays.asList(user1,user2));
		
		
		
	}
	
	@Bean	
	public FilterRegistrationBean crosFilter(){
		UrlBasedCorsConfigurationSource source=new UrlBasedCorsConfigurationSource();
		CorsConfiguration configuration=new CorsConfiguration();
		configuration.setAllowCredentials(true);
		configuration.addAllowedOrigin("*");
		configuration.addAllowedHeader("*");
		configuration.addAllowedMethod("*");
		source.registerCorsConfiguration("/**",configuration);
		FilterRegistrationBean bean=new FilterRegistrationBean(new org.springframework.web.filter.CorsFilter(source));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
		
		
	}

	
	
}
