package com.poc.multitency.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.poc.multitency.config.TenantContext;
import com.poc.multitency.domain.Faculty;
import com.poc.multitency.security.domain.MultiTencyUser;
import com.poc.multitency.service.FacultyService;

@RestController
public class FacultyController {
	@Autowired
	private FacultyService facultyService;

	@GetMapping("/api/viewlist")
	public List<Faculty> addEmployee(@AuthenticationPrincipal Principal principal) {

		MultiTencyUser user = (MultiTencyUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String tenant = user.getTenant();
		System.out.println("Tenant name :" + tenant);
		TenantContext.setCurrentTenant(tenant);
		List<Faculty> list = facultyService.getAllFaculties();
		return list;
	}

	@GetMapping("/api/userdetails")
	public MultiTencyUser userDetails() {
		MultiTencyUser user = (MultiTencyUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user;
	}

	@GetMapping("/api/{tenant}/update")
	public void updateEmployee(@PathVariable("tenant") String tenant) {
		TenantContext.setCurrentTenant(tenant);
		System.out.println("Tenant name :" + tenant);
		Faculty faculty = new Faculty.FacultyBuilder().withName("Lakshman A").withEmail("lakshman.a@updated.com")
				.withMobile("9036102111").withQualification("MCA").build();
		facultyService.updateFaculty(faculty);
	}

}
