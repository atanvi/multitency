package com.poc.multitency.security.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.poc.multitency.security.domain.MultiTencyUser;

public interface MultitencyRepo extends MongoRepository<MultiTencyUser,String>{
	
		MultiTencyUser findByEmail(String email);

}
