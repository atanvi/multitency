package com.poc.multitency.security.domain;

import org.springframework.data.annotation.Id;

public class Role {

	@Id
	private String role;
	private String roleDescription;
	
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getRoleDescription() {
		return roleDescription;
	}
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Role [role=");
		builder.append(role);
		builder.append(", roleDescription=");
		builder.append(roleDescription);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}