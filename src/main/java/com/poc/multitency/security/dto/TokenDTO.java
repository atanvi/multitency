package com.poc.multitency.security.dto;

public class TokenDTO {

	private String token;
	private String userName;
	private String role;
	private String companyId;
	private String tenantName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	@Override
	public String toString() {
		return "TokenDTO [token=" + token + ", userName=" + userName + ", role=" + role + ", companyId=" + companyId
				+ ", tenantName=" + tenantName + "]";
	}
	
	
	
}
