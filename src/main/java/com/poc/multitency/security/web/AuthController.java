/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.poc.multitency.security.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.poc.multitency.dto.UserAuthDTO;
import com.poc.multitency.security.config.JwtUtil;
import com.poc.multitency.security.domain.MultiTencyUser;
import com.poc.multitency.security.dto.TokenDTO;
import com.poc.multitency.security.service.MultitencyUserService;

/**
 *
 * @author pradeepkm
 */
@RestController
@RequestMapping("/auth")
public class AuthController {

	public static final Logger logger = LoggerFactory.getLogger(AuthController.class);
	
	@Autowired
	private MultitencyUserService multitencyUserService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public TokenDTO login(@RequestBody UserAuthDTO userDetails) {
		String userName = userDetails.getEmail();
		String passwd = userDetails.getPassword();
		logger.info("logging in with userName {} ", userName);
		MultiTencyUser obj =verifyPasswd(userName, passwd);
		
		TokenDTO token = new TokenDTO();
		if(obj!=null){
			token.setUserName(obj.getFirstName());
			token.setRole(obj.getRole().getRoleDescription());
			token.setToken(JwtUtil.createToken(obj));
			token.setTenantName(obj.getTenant());
		}	
		return token;
	

	}

	private MultiTencyUser verifyPasswd(String userName, String passwd) {
		MultiTencyUser retObject = multitencyUserService.findByEmail(userName);
		System.out.println("Return object :"+retObject);
		if (retObject != null) {
			if (retObject.getPassword().equals(passwd))
				return retObject;
		}

		return null;
	}

}
