/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.poc.multitency.security.web;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.poc.multitency.security.domain.MultiTencyUser;

/**
 *
 * @author pradeepkm
 */

@RestController
@RequestMapping("/api")
public class APIController {
    @RequestMapping(value="/resource", method = RequestMethod.GET)
    public Map<String,Object> home() {
        Map<String, Object> model = new HashMap<>();
        model.put("id", UUID.randomUUID().toString() );
        model.put("content", "HelloWorld");
        return model;
    }

    @RequestMapping(value="/user", method = RequestMethod.GET)
    public Principal user(Principal user) {
    	System.out.println(user.getClass());
    	MultiTencyUser user1=(MultiTencyUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	System.out.println("Tenant name : ------------------ "+user1.getTenant());
        return user;
    }
}
