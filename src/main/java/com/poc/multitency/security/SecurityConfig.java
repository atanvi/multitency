/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.poc.multitency.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.poc.multitency.security.config.JwtAuthProcessingFilter;
import com.poc.multitency.security.config.JwtAuthenticationEntryPoint;
import com.poc.multitency.security.config.JwtAuthenticationProvider;
import com.poc.multitency.security.config.JwtAuthenticationSuccessHandler;

/**
 *
 * @author pradeepkm
 */
@Configuration
@EnableWebSecurity
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
	 public static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);
	    
	    @Autowired
	    private JwtAuthenticationProvider jwtAuthenticationProvider;
	    
	    @Autowired
	    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	    
	    @Bean
	    public JwtAuthProcessingFilter authenticationTokenFilterBean() throws Exception {
	        JwtAuthProcessingFilter filter = new JwtAuthProcessingFilter();
	        filter.setAuthenticationManager(authenticationManagerBean());
	        filter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
	        return filter;
	    }
	    
	    @Bean
	    @Override
	    public AuthenticationManager authenticationManagerBean() throws Exception {
	        return super.authenticationManagerBean();
	    }
	    
	    @Override
	    protected void configure(AuthenticationManagerBuilder authManagerBuilder ) throws Exception {
	    	LOGGER.info("\n\n\n\nJWT NOTE: configuring authentication provider {}\n\n\n\n", this.jwtAuthenticationProvider);
	        authManagerBuilder.authenticationProvider(this.jwtAuthenticationProvider);
	    }
	    
	    @Override
	    protected void configure(HttpSecurity httpSecurity ) throws Exception {
	        
	    	LOGGER.info("\n\n\n\nJWT NOTE: authentication entry point {}, authentication provider {} \n\n\n\n", 
	                jwtAuthenticationEntryPoint, jwtAuthenticationProvider);
	        
	        httpSecurity.csrf().disable()
	                    .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and() 
	                    //dont create sessions
	                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
	                    .authorizeRequests()
	                    .antMatchers("/").permitAll()
	                    .antMatchers("/*.html").permitAll()
	                    .antMatchers("/favicon.ico").permitAll()
	                    .antMatchers("**/*.html").permitAll()
	                    .antMatchers("**/*.css").permitAll()
	                    .antMatchers("**/*.js").permitAll()
//			NOTE: The following way of pattern matching is preferred,  however this needs to be tested  
//	                    .antMatchers(HttpMethod.GET,
//	                            "/",
//	                            "/*.html",
//	                            "/favicon.ico",
//	                            "/**/*.html",
//	                            "/**/*.css",
//	                            "/**/*.js").permitAll()
	                    .antMatchers("/auth/**").permitAll()
	                    .anyRequest().authenticated();
	        
	        httpSecurity.addFilterBefore(authenticationTokenFilterBean(), 
	                UsernamePasswordAuthenticationFilter.class);
	        
	        httpSecurity.headers().cacheControl();
	    }   
	
}
