/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.poc.multitency.security.config;

import org.springframework.security.core.AuthenticationException;

/**
 *
 * @author pradeepkm
 */
public class JwtAuthenticationException extends AuthenticationException{
    private static final long serialVersionUID = 1L;

	public JwtAuthenticationException(String msg) {
        super(msg);
    }
    
}
