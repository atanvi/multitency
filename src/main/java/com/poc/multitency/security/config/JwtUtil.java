/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.poc.multitency.security.config;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import com.poc.multitency.security.domain.MultiTencyUser;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 *
 * @author pradeepkm
 */
public class JwtUtil {


	 public static final Logger LOGGER = LoggerFactory.getLogger(JwtUtil.class);
	    
	    //read from properties...
	    private static final String secretKey = "aboycandoeverythingforgirl";
	    private static final String tenantKey = "tenant";
		private static final String emailKey = "email";
		private static final String idKey = "id";
		private static final String FIRST_NAME = "firstName";
		private static final String LAST_NAME = "lastName";
	  
	   

	    
	    
		public static MultiTencyUser parseToken(String token) throws AuthenticationException {
			try {
				Claims body = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();
			
				String email = (String) body.get(emailKey);
				String tenant = (String) body.get(tenantKey);
				String id = (String) body.get(idKey);
				String firstName = (String) body.get(FIRST_NAME);
				String lastName = (String) body.get(LAST_NAME);
				Set<GrantedAuthority> authorities = new HashSet<>();
			
				
				MultiTencyUser userDetails = new MultiTencyUser();
				userDetails.setEmail(email);
				userDetails.setAuthorities(authorities);
				userDetails.setTenant(tenant);
				userDetails.setId(id);
				userDetails.setFirstName(firstName);
				userDetails.setLastName(lastName);

				return userDetails;
			} catch (JwtException | ClassCastException e) {
				LOGGER.error("Error while parsing user supplied token {}", e.getMessage());
				throw new JwtAuthenticationException("Token parse error...");
			}

		}

		public static String createToken(MultiTencyUser userDetails) throws AuthenticationException {
			if (userDetails != null) {
				Claims claims = Jwts.claims().setSubject(userDetails.getEmail());
				claims.put(emailKey, userDetails.getEmail());
				claims.put(idKey, userDetails.getId());
				claims.put(tenantKey, userDetails.getTenant());
				claims.put(FIRST_NAME, userDetails.getFirstName());
				claims.put(LAST_NAME, userDetails.getLastName());
				String jwt = Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secretKey).compact();

				return jwt;
			}
			throw new JwtAuthenticationException("Could not create JWT token...");
		}
}
