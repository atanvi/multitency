package com.poc.multitency.security.service;

import org.springframework.stereotype.Service;

import com.poc.multitency.security.domain.MultiTencyUser;

@Service
public interface MultitencyUserService {
	
			
			
			public MultiTencyUser findByEmail(String email);

}
