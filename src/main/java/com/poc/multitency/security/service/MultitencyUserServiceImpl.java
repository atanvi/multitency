package com.poc.multitency.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.multitency.security.domain.MultiTencyUser;
import com.poc.multitency.security.repo.MultitencyRepo;

@Service
public class MultitencyUserServiceImpl implements MultitencyUserService {

	@Autowired MultitencyRepo multitencyRepo;
	
	
	@Override
	public MultiTencyUser findByEmail(String email) {
	
		return multitencyRepo.findByEmail(email);
	}

}
